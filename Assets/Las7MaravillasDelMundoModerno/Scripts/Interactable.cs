﻿using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    public UnityEvent OnOverEvent;
    public UnityEvent OnOffEvent;
    public UnityEvent OnClickEvent;

    public GameObject controllerScene;


    bool m_bDown, m_blastDown;

    public GameObject objeto;

    public void Down()
    {
        m_bDown = true;
    }

    public void Clicked()
    {
        OnClickEvent.Invoke();
    }

    void Update()
    {
        if (m_bDown && !m_blastDown)
        {
            OnOverEvent.Invoke();
        }

        if (!m_bDown && m_blastDown)
        {
            OnOffEvent.Invoke();
        }

        m_blastDown = m_bDown;
        m_bDown = false;

    }

    public void TamanoAumentado(float tam)
    {
        this.gameObject.transform.localScale = new Vector3(tam, tam, tam);

    }

    public void TamanoOriginal(float tam)
    {
        this.gameObject.transform.localScale = new Vector3(tam, tam, tam);
    }

    public void AbrirBubbleInformation()
    {
        if (objeto.activeSelf)
            objeto.SetActive(false);
        else
            objeto.SetActive(true);
            
    }

    public void RotarIzq()
    {
        objeto.gameObject.transform.Rotate(0, -1.5f, 0);
    }

    public void RotarDer()
    {
        objeto.gameObject.transform.Rotate(0, 1.5f, 0);
    }

    public void CambiarEscena(string scena)
    {
        controllerScene.GetComponent<SceneChanger>().FadeToScene(scena);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
