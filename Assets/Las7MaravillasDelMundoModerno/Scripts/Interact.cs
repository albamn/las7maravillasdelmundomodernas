﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    public int scene = 0; //alguno de las maravillas. Si scene == 0, es el menu principal
    private bool bClicked = false;

    // Update is called once per frame
    void Update()
    {
        RaycastHit raycast;

        if (scene != 1) MaravillasInteract();
        else MenuMundoInteract();

        if (Physics.Raycast(new Ray(transform.position, transform.forward), out raycast, Mathf.Infinity))
        {
            GameObject target = raycast.collider.gameObject;
            Interactable interactable = target.GetComponent<Interactable>();
            if (interactable != null)
            {
                interactable.Down();
                if (bClicked)
                {
                    interactable.Clicked();
                }
            }
        }
    }

    private void MenuMundoInteract()
    {
        //Queremos que sea un pressed para que pueda rotar el mundo sin necesidad de estar haciendo click todo el rato
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.One) || Input.GetMouseButtonDown(0))
        {
            bClicked = true;
        }
        else if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetUp(OVRInput.Button.One) || Input.GetMouseButtonUp(0))
        {
            bClicked = false;
        }
    }

    private void MaravillasInteract()
    {
        bClicked = false;
        //Se comportara como un click
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.One) || Input.GetMouseButtonDown(0))
        {
            bClicked = true;
        }
    }
}